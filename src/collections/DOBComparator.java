package collections;

import java.util.Comparator;

public class DOBComparator implements Comparator<Student>{

	@Override
	public int compare(Student stu1, Student stu2) {
		
		if (stu1.getDateOfBirth().before(stu2.getDateOfBirth()))
			return -1;
		else if (stu1.getDateOfBirth().after(stu2.getDateOfBirth()))
			return 1;
		else 
			return 0;

	}

}
