package collections;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class MainClass {
	static Map<String, HashSet<Character>> map = new HashMap<String, HashSet<Character>>();
	public static void countingChars(){
		Scanner sc = new Scanner(System.in);
		String inputString = sc.nextLine();
		HashSet<Character> uniqueCharacters = new HashSet<Character>();
		for (char chars : inputString.toCharArray()) {
			uniqueCharacters.add(chars);
		}
		System.out.println("Unique character count is"+uniqueCharacters.size());
	}
	public static void countingCharactersWithCaching(){
		
		//HashSet<Character> uniqueCharacters = new HashSet<Character>();
		Scanner sc = new Scanner(System.in);
		String inputString = sc.nextLine();
	
		
		String[] words = inputString.split(" ");
		
		for (String word : words) {
			if(!map.containsKey(word))
			{
				HashSet<Character> uniqueCharacters = new HashSet<Character>();
				for (char chars : word.toCharArray()) {
					uniqueCharacters.add(chars);
				}
				map.put(word, uniqueCharacters);
			}
		}
		
		Iterator it = map.entrySet().iterator();
		while(it.hasNext())
		{
			Map.Entry uniques = (Map.Entry)it.next(); 
			//System.out.println(uniques.getKey()+"-->"+ uniques.getValue()+"\n");
			HashSet<Character> uniqueChars = (HashSet<Character>)uniques.getValue();
			System.out.println(uniques.getKey()+"-->"+ uniqueChars.size());
			
		}
	}
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("1.Counting chars\n2.COunting chars with caching");
		int choice = Integer.parseInt(scan.nextLine());
		switch ((choice)) {
		case 1:
			countingChars();
			break;
		case 2:
			countingCharactersWithCaching();
			break;

		default:
			break;
		}
	}
	
	
}
