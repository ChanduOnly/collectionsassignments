package collections;

import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetMain {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {

		Student stu1 = new Student("John", 10, "John's creek", new Date(1992,8,8));
		Student stu2 = new Student("Tom", 13, "Colonial", new Date(1994,8,18));
		Student stu3 = new Student("Kris", 12, "Preston creek", new Date(1996,8,9));
		Student stu4 = new Student("Tuna", 11, "Wesley", new Date(1998,8,7));
		
		
		Set<Student> students = new TreeSet<Student>();
		students.add(stu1);
		students.add(stu2);
		students.add(stu3);
		students.add(stu4);
		
		Iterator<Student> it = students.iterator();
		while(it.hasNext())
			System.out.println(it.next().getName());
		
		System.out.println("DOB Comparator");
		Set<Student> students2 = new TreeSet<Student>(new DOBComparator());
		students2.add(stu4);
		students2.add(stu3);
		students2.add(stu1);
		students2.add(stu2);
		
		Iterator<Student> it2 = students2.iterator();
		while(it2.hasNext())
			System.out.println(it2.next().getName());
		
		
	}

}
