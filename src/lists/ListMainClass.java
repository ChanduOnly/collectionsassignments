package lists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import com.sun.xml.internal.bind.v2.runtime.reflect.ListIterator;



public class ListMainClass {

	public static void stringList(){
		List<String> names = new ArrayList<String>();
		
		names.add("abc");
		names.add("def");
		names.add("bcd");
		
		Collections.sort(names, new NameComparator());
		
		for (String string : names) {
			System.out.println(string);
		}
	} 
	
	public static void customList(){
		List<Student> Students = new ArrayList<Student>();
		
		Student stu1 = new Student("John", 10, "John's creek", new Date(1992,8,8));
		Student stu2 = new Student("Tom", 13, "Colonial", new Date(1994,8,18));
		Student stu3 = new Student("Kris", 12, "Preston creek", new Date(1996,8,9));
		Student stu4 = new Student("Tuna", 11, "Wesley", new Date(1998,8,7));
		
		Students.add(stu1);
		Students.add(stu1);
		Students.add(stu2);
		Students.add(stu3);
		Students.add(stu4);
		
		Students.sort(new StudentListComparator());
		
		for (Student student : Students) {
			System.out.println(student.getName());
		}
	}
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("1.String List\n2.Custom Object list");
		int choice = Integer.parseInt(scan.nextLine());
		switch ((choice)) {
		case 1:
			stringList();
			break;
		case 2:
			customList();
			break;

		default:
			break;
		}
	}

}
