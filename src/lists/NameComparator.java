package lists;

import java.util.Comparator;

public class NameComparator implements Comparator<String>{

	@Override
	public int compare(String str1, String str2) {
		return str1.compareTo(str2);
	}

}
