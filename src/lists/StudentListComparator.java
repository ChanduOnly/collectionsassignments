package lists;

import java.util.Comparator;

public class StudentListComparator implements Comparator<Student> {

	@Override
	public int compare(Student stu1, Student stu2) {
		if (stu1.getAge()>stu2.getAge())
			return 1;
		else if (stu1.getAge() < stu2.getAge())
			return -1;
		else 
			return 0;
		
	}

}
