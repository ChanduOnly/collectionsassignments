package map;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import collections.Student;

public class MapMainClass {

	public static void main(String[] args) {
		Map<String, Student> Students = new TreeMap<String, Student>();
		
		Student stu1 = new Student("John", 10, "John's creek", new Date(1992,8,8));
		Student stu2 = new Student("Tom", 13, "Colonial", new Date(1994,8,18));
		Student stu3 = new Student("Kris", 12, "Preston creek", new Date(1996,8,9));
		Student stu4 = new Student("Tuna", 11, "Wesley", new Date(1998,8,7));
		
		Students.put("Leader", stu4);
		Students.put("Manager", stu2);
		Students.put("Aeammate", stu1);
		Students.put("Deammate2", stu3);
		Students.put("Beammate3", stu1);
		Students.put("Beammate3", stu3);
		
		//checking
		Iterator it = Students.entrySet().iterator();
		while(it.hasNext()){
		Map.Entry<String, Student> ite = (Map.Entry<String, Student>)it.next();
		System.out.println(ite.getValue().getName());
		}
		
		
	}

}
